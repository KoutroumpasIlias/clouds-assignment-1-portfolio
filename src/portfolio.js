const header = {
  // all the properties are optional - can be left empty or deleted
  homepage: 'https://rajshekhar26.github.io/cleanfolio',
  title: 'Clouds Portfolio Example',
}

const about = {
  // all the properties are optional - can be left empty or deleted
  name: 'Koutroumpas Ilias',
  role: 'Software Engineer',
  description:
    'Currently following a Clouds course at EURECOM',
  resume: 'https://example.com',
  social: {
    linkedin: 'https://www.linkedin.com/in/ilias-koutroumpas-b64757203/',
    github: 'https://github.com/IliasKoutroumpas',
  },
}

const projects = [
  // projects can be added an removed
  // if there are no projects, Projects section won't show up
  {
    name: 'RoadrunnerDM',
    description:
      'Delivery manager application for effectively and fairly grouping orders based on location',
    stack: ['Laravel/PHP', 'MySQL/SQL', 'ReactJS/Redux'],
    sourceCode: 'https://github.com/IliasKoutroumpas',
    livePreview: 'https://roadrunnerdm.com/',
  },
  {
    name: 'N-HiTec',
    description:
      'Junior Entreprise (JE) based in Belgium and under the University of Liege (ULiege). Consulting for web, mobile development and data sciense projects.',
    stack: ['Laravel/PHP', 'Linux/CentOS', 'CSS/Bootstrap', 'Flutter/Dart', 'JQuery/JavaScript', 'MongoDB/NoSQL'],
    sourceCode: 'https://github.com/nhitec',
    livePreview: 'https://nhitec.com/fr',
  },
  {
    name: 'DMC - Pontos',
    description:
      'Cargo ship live tracking application. Ship trajectory scheduling and management.',
    stack: ['HTML3', 'Bootstrap/CSS', 'JQuery/JS', '.NET/C#', 'MicrosoftServer/SQL'],
    sourceCode: '#',
    livePreview: 'http://my-pontos.com',
  },
]

const skills = [
  // skills can be added or removed
  // if there are no skills, Skills section won't show up
  'HTML',
  'CSS',
  'JavaScript',
  'TypeScript',
  'React',
  'Redux',
  'Material UI',
  'Laravel',
  'Git',
  'CI/CD',
  'Jest',
  'C/C++',
  'Java',
  'Flex/Bison',
  '...'
]

const contact = {
  // email is optional - if left empty Contact section won't show up
  email: 'koutroum@eurecom.fr',
}

export { header, about, projects, skills, contact }
